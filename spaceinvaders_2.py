# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 13:39:55 2018

@author: fabien.mallol
"""
from Tkinter import *
from tkFont import Font
import random

window = None
canvas = None
windowWidth = None
windowHeight = None
canvasWidth = None
canvasHeight = None
intscore = 0
intvie = 3
score = None
canScore = None
aLignes = 4
aColonnes = 8
nbAliens = None
listAliens = []
shoot = []
vaisseau = None
direction = 1
tir = None
tirAlien = None
listBaricades = []
victoire = False
perdu = False
dernierTireur = None




class missile:

    dimX = 10
    dimY = 20
    pas = 30
    pasEnnemi = 15

    def __init__(self, coords):
        self.coords = coords

    def makeTir(self):
        global canvas
        self.xTir = self.coords[2] - ((self.coords[2] - self.coords[0]) / 2.0)
        self.yTir = self.coords[1]
        self.tir = canvas.create_rectangle(self.xTir - missile.dimX / 2, self.yTir - missile.dimY, self.xTir + missile.dimX / 2, self.yTir, fill='YELLOW')

    def moveTir(self):
        global canvas, canvasWidth, canvasHeight, window

        self.posTir = canvas.coords(self.tir)

        if self.posTir[3] <= 0 and self.tir:
            window.after_cancel(self.tir)
            canvas.delete(self.tir)
            killBoum()

        elif contact(self.posTir):
            window.after_cancel(self.tir)
            canvas.delete(self.tir)
            killBoum()

        elif contactBaricades(self.posTir, 'bas'):
            window.after_cancel(self.tir)
            canvas.delete(self.tir)
            killBoum()

        else:
            canvas.coords(self.tir, self.posTir[0], self.posTir[1] - missile.pas, self.posTir[2], self.posTir[3] - missile.pas)
            window.after(20, self.moveTir)

    def makeTirAlien(self):
        global canvas, victoire, perdu
        if not victoire and not perdu:
            self.xTir = self.coords[2] - ((self.coords[2] - self.coords[0]) / 2.0)
            self.yTir = self.coords[3]
            self.tir = canvas.create_rectangle(self.xTir - missile.dimX / 2, self.yTir, self.xTir + missile.dimX / 2, self.yTir + missile.dimY, fill='RED')

    def moveTirAlien(self):
        global canvas, canvasHeight, window

        self.posTir = canvas.coords(self.tir)

        if self.posTir[1] >= canvasHeight and self.tir:
            window.after_cancel(self.tir)
            canvas.delete(self.tir)
            killBoumAlien()

        elif contactBaricades(self.posTir, 'haut'):
            window.after_cancel(self.tir)
            canvas.delete(self.tir)
            killBoumAlien()

        elif contactVaisseau(self.posTir):
            window.after_cancel(self.tir)
            canvas.delete(self.tir)
            killVaisseau()

        else:
            canvas.coords(self.tir, self.posTir[0], self.posTir[1] + missile.pasEnnemi, self.posTir[2], self.posTir[3] + missile.pasEnnemi)
            window.after(20, self.moveTirAlien)


class aliens:

    def __init__(self, x0, y0, x1, y1):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.varDestroy = False

    def draw(self):
        global canvas
        self.rect = canvas.create_rectangle(self.x0, self.y0, self.x1, self.y1, fill='GREEN')

    def move(self, mvx0, mvy0, mvx1, mvy1):
        global canvas
        canvas.coords(self.rect, mvx0, mvy0, mvx1, mvy1)

    def getPos(self):
        global canvas
        return canvas.coords(self.rect)

    def destroy(self):
        return self.varDestroy

    def kill(self):
        global canvas
        canvas.delete(self.rect)
        self.varDestroy = True


class baricades:

    def __init__(self, x0, y0, x1, y1):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.varDestroy = False

    def draw(self):
        global canvas
        self.rect = canvas.create_rectangle(self.x0, self.y0, self.x1, self.y1, fill='white')

    def getPos(self):
        global canvas
        return canvas.coords(self.rect)

    def destroy(self):
        return self.varDestroy

    def kill(self):
        global canvas
        canvas.delete(self.rect)
        self.varDestroy = True


def createGUI():
    global window, canvas, windowWidth, windowHeight, canvasWidth, canvasHeight, canScore, score, intscore, intvie

    window = Tk()
    window.title("Space Invaders")
    windowWidth = window.winfo_screenwidth() - 100
    windowHeight = window.winfo_screenheight() - 100
    window.geometry(str(windowWidth) + 'x' + str(windowHeight))

    canvasWidth = windowWidth * (7.0 / 8.0)
    canvasHeight = windowHeight * (11.0 / 12.0)
    buttonWidth = windowWidth * (1.0 / 8.0)
    canvas = Canvas(window, width=canvasWidth, height=canvasHeight, background='black')

    score = StringVar()
    score.set("Score : %d | Vie : %d" % (intscore, intvie))
    canScore = Label(window, textvar=score)
    quitter = Button(window, text="Quitter", command=window.destroy)
    restart = Button(window, text="Restart", command=restartGame)

    window.bind("<Key>", key)

    canScore.pack()
    canvas.pack(anchor='sw', side='bottom', padx=5, pady=5)
    quitter.place(x=windowWidth - buttonWidth / 1.5, y=windowHeight / 2 - 30)
    restart.place(x=windowWidth - buttonWidth / 1.5, y=windowHeight / 2 + 30)


def key(event):
    global canvas, canvasWidth, canvasHeight, vaisseau, shoot
    touche = event.keysym

    pas = 20
    coordVaisseau = canvas.coords(vaisseau)
    if touche == 'Right':
        if not coordVaisseau[2] + 15 >= canvasWidth:
            canvas.coords(vaisseau, coordVaisseau[0] + pas, coordVaisseau[1], coordVaisseau[2] + pas, coordVaisseau[3])

    if touche == 'Left':
        if not coordVaisseau[0] - 15 <= 0:
            canvas.coords(vaisseau, coordVaisseau[0] - pas, coordVaisseau[1], coordVaisseau[2] - pas, coordVaisseau[3])

    if touche == 'space':
        boum(coordVaisseau)


def makeVaisseau():
    global canvas, canvasWidth, canvasHeight, vaisseau

    dimensionVaisseau = 45

    vaisseau = canvas.create_rectangle((canvasWidth / 2) - dimensionVaisseau / 2, canvasHeight - 10 - dimensionVaisseau, canvasWidth / 2 + dimensionVaisseau / 2, canvasHeight - 10, fill='BISQUE')


def makeAlien():
    global listAliens, nbAliens, aLignes, aColonnes

    nbAliens = aLignes * aColonnes
    dimensionAlien = 50

    for i in range(1, aColonnes + 1):
        for j in range(1, aLignes + 1):
            listAliens.append(aliens(60 * i, 60 * j, 60 * i + dimensionAlien, 60 * j - dimensionAlien))

    for k in range(nbAliens):
        listAliens[k].draw()


def moveAlien():
    global canvas, listAliens, canvasWidth, canvasHeight, nbAliens, direction, window, victoire, perdu
    pas = 5
    pasY = 20
    posY = 0
    moins = 0
    plus = nbAliens - 1

    if not victoire and not perdu:
        while moins < nbAliens and listAliens[moins].destroy():
            moins += 1
        if moins < nbAliens:
            alienGauche = listAliens[moins].getPos()[0]
            while listAliens[plus].destroy():
                plus -= 1
            alienDroite = listAliens[plus].getPos()[2]
            if alienGauche - pas <= 0:
                direction = 1
                posY += pasY

            elif alienDroite + pas >= canvasWidth:
                direction = -1
                posY += pasY

            for i in range(nbAliens):
                if not listAliens[i].destroy():
                    coordAlien = listAliens[i].getPos()
                    listAliens[i].move(coordAlien[0] + direction * pas, coordAlien[1] + posY, coordAlien[2] + direction * pas, coordAlien[3] + posY)

            window.after(20, moveAlien)

        else:
            victoireFct()


def makeBaricades():
    global listBaricades, canvasWidth, canvasHeight

    nbBaricades = 4
    epaisseur = 3
    largeur = 6
    nbBloc = nbBaricades*epaisseur*largeur
    dimensionBloc = 20
    deb = (canvasWidth - (largeur*nbBaricades*dimensionBloc + 3 * (canvasWidth/4-largeur*dimensionBloc)))/2

    for i in range(0, nbBaricades):
        for j in range(0, largeur):
            for k in range(0, epaisseur):
                listBaricades.append(baricades(deb+j*dimensionBloc+i*(canvasWidth/4), canvasHeight - 75-k*dimensionBloc - dimensionBloc, deb+dimensionBloc+j*dimensionBloc+i*(canvasWidth/4), canvasHeight - 75 - k*dimensionBloc))

    for m in range(nbBloc):
        listBaricades[m].draw()


def boum(coords):
    global tir
    if tir is None:
        tir = missile(coords)
        tir.makeTir()
        tir.moveTir()


def killBoum():
    global tir
    del tir
    tir = None


def premierAlien():
    global listAliens, aColonnes
    valren = []
    for i in range(aColonnes):
        j = 4*i
        if not listAliens[j].destroy():
            while j < 4*(i+1) and not listAliens[j].destroy():
                j += 1

            valren.append(j-1)

    return valren


def baricadesHaut():
    global listBaricades
    colonnes = 24
    valren = []
    for i in range(colonnes):
        j = 3*i
        if not listBaricades[j].destroy():
            while j < 3*(i+1) and not listBaricades[j].destroy():
                j += 1

            valren.append(j-1)

    return valren


def baricadesBas():
    global listBaricades
    colonnes = 24
    valren = []
    for i in range(colonnes):
        j = 3*i+2
        if not listBaricades[j].destroy():
            while j > 3*(i-1)+2 and not listBaricades[j].destroy():
                j -= 1

            valren.append(j+1)

    return valren


def contact(coordsTir):
    global canvas, listAliens, aLignes, aColonnes, nbAliens, dernierTireur
    destruction = False
    i = 0
    indicesPremiers = premierAlien()
    while i < len(indicesPremiers) and not destruction:
        ite = indicesPremiers[i]
        coordsA = listAliens[ite].getPos()
        if ((coordsTir[3] >= coordsA[1]) and (coordsTir[1] <= coordsA[3])) and ((coordsTir[2] >= coordsA[0]) and (coordsTir[0] <= coordsA[2])):
            destruction = True
            listAliens[ite].kill()
            if ite == dernierTireur:
                updateScore(25)
            else:
                updateScore(10)

        else:
            i += 1

    return destruction


def contactBaricades(coordsTir, pos):
    global canvas, listBaricades
    destruction = False
    i = 0
    if pos == 'haut':
        indicesPremiers = baricadesHaut()
    elif pos == 'bas':
        indicesPremiers = baricadesBas()

    while i < len(indicesPremiers) and not destruction:
        ite = indicesPremiers[i]
        coordsA = listBaricades[ite].getPos()
        if ((coordsTir[3] >= coordsA[1]) and (coordsTir[1] <= coordsA[3])) and ((coordsTir[2] >= coordsA[0]) and (coordsTir[0] <= coordsA[2])):
            destruction = True
            listBaricades[ite].kill()

        else:
            i += 1

    return destruction


def contactVaisseau(coordsTir):
    global canvas, vaisseau
    coordsVaisseau = canvas.coords(vaisseau)
    destruction = False
    if ((coordsTir[3] >= coordsVaisseau[1]) and (coordsTir[1] <= coordsVaisseau[3])) and ((coordsTir[2] >= coordsVaisseau[0]) and (coordsTir[0] <= coordsVaisseau[2])):
        destruction = True

    return destruction


def tirEnnemi():
    global listAliens, window, tirAlien, victoire, perdu, dernierTireur
    if not victoire and not perdu:
        indicesPremiers = premierAlien()
        randAlien = random.sample(indicesPremiers, 1)[0]
        dernierTireur = randAlien
        alienCoords = listAliens[randAlien].getPos()
        tirAlien = missile(alienCoords)
        tirAlien.makeTirAlien()
        tirAlien.moveTirAlien()

        delay = random.randrange(1400, 2500, 10)
        window.after(delay, tirEnnemi)
    else:
        window.after_cancel(tirEnnemi)


def killBoumAlien():
    global tirAlien
    del tirAlien
    tirAlien = None


def updateScore(inc):
    global score, intscore, intvie
    intscore += inc
    score.set("Score : %d | Vie : %d" % (intscore, intvie))


def killVaisseau():
    global intvie
    intvie -= 1
    updateScore(0)
    if intvie == 0:
        perduFct()


def perduFct():
    global window, canvas, canvasWidth, canvasHeight, vaisseau, perdu, intscore
    perdu = True
    window.after_cancel(tirEnnemi)
    window.after_cancel(moveAlien)
    canvas.delete('all')
    canvas.create_text(canvasWidth/2,canvasHeight/2,text="PERDU !\nAvec %d points" % intscore, fill="white", font="Times 60")


def victoireFct():
    global window, canvas, vaisseau, victoire, canvasWidth, canvasHeight, intscore
    victoire = True
    window.after_cancel(tirEnnemi)
    window.after_cancel(moveAlien)
    canvas.delete('all')
    canvas.create_text(canvasWidth/2,canvasHeight/2,text="GAGNE !\nAvec %d points" % intscore, fill="white", font="Times 60")


def restartGame():
    global window, canvas, victoire, perdu, listAliens, listBaricades, shoot, vaisseau, tir, tirAlien, intscore, intvie, score
    window.after_cancel(tirEnnemi)
    window.after_cancel(moveAlien)
    canvas.delete("all")
    victoire = False
    perdu = False
    listAliens = []
    listBaricades = []
    shoot = []
    del vaisseau
    #vaisseau = None
    del tir
    tir = None
    del tirAlien
    tirAlien = None
    intscore = 0
    intvie = 3
    score.set("Score : %d | Vie : %d" % (intscore, intvie))

    makeAlien()
    makeBaricades()
    makeVaisseau()
    moveAlien()
    tirEnnemi()


createGUI()
makeAlien()
makeBaricades()
makeVaisseau()
moveAlien()
tirEnnemi()
window.mainloop()
