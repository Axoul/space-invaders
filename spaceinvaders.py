# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 13:39:55 2018

@author: fabien.mallol
"""
from Tkinter import *
from tkFont import Font

window = None
canvas = None
windowWidth = None
windowHeight = None
canvasWidth = None
canvasHeight = None
score = 0
aLignes = 4
aColonnes = 8
nbAliens = None
aliens = []
shoot = []
vaisseau = None
direction = 1
tir = None


def createGUI():
    global window, canvas, windowWidth, windowHeight, canvasWidth, canvasHeight, score

    window = Tk()
    window.title("Space Invaders")
    windowWidth = window.winfo_screenwidth() - 100
    windowHeight = window.winfo_screenheight() - 100
    window.geometry(str(windowWidth) + 'x' + str(windowHeight))

    canvasWidth = windowWidth * (7.0 / 8.0)
    canvasHeight = windowHeight * (11.0 / 12.0)
    buttonWidth = windowWidth * (1.0 / 8.0)
    canvas = Canvas(window, width=canvasWidth, height=canvasHeight, background='black')

    score = Label(window, text="score : " + str(score))
    quitter = Button(window, text="Quitter", command=window.destroy)
    restart = Button(window, text="Restart", command=restartGame)

    window.bind("<Key>", key)

    score.pack()
    canvas.pack(anchor='sw', side='bottom', padx=5, pady=5)
    quitter.place(x=windowWidth - buttonWidth / 1.5, y=windowHeight / 2 - 30)
    restart.place(x=windowWidth - buttonWidth / 1.5, y=windowHeight / 2 + 30)


def key(event):
    global canvas, canvasWidth, canvasHeight, vaisseau, shoot
    touche = event.keysym

    pas = 20
    coordVaisseau = canvas.coords(vaisseau)
    if touche == 'Right':
        if not coordVaisseau[2] + 15 >= canvasWidth:
            canvas.coords(vaisseau, coordVaisseau[0] + pas, coordVaisseau[1], coordVaisseau[2] + pas, coordVaisseau[3])

    if touche == 'Left':
        if not coordVaisseau[0] - 15 <= 0:
            canvas.coords(vaisseau, coordVaisseau[0] - pas, coordVaisseau[1], coordVaisseau[2] - pas, coordVaisseau[3])

    if touche == 'space':
        boum(coordVaisseau)

def boum(coords):
    global tir
    if tir == None:
        tir = missile(coords)
        tir.makeTir()
        tir.moveTir()

def killBoum():
    global tir
    del tir
    tir = None

def contact(coordsTir):
    global canvas, aliens, aLignes, aColonnes
    i = 0
    destruction = False
    while i < len(aliens) and not destruction:
        print i
        if (i < len(aliens)-1):
            if aliens[i] is not None:
                if (i+1)%4 == 0:
                    if (coordsTir[3] <= canvas.coords(aliens[i])[1]) and ((coordsTir[2] >= canvas.coords(aliens[i])[0]) and (coordsTir[0] <= canvas.coords(aliens[i])[2])):
                        print 'boum'
                        destruction = True
                        canvas.delete(aliens[i])
                        aliens[i] = None
                    else:
                        i+=1
                else:
                    i+=1
            else:
                i+=1
        else:
            if aliens[i] is not None:
                if (coordsTir[3] <= canvas.coords(aliens[i])[1]) and ((coordsTir[2] >= canvas.coords(aliens[i])[0]) and (coordsTir[0] <= canvas.coords(aliens[i])[2])):
                    print 'boum'
                    destruction = True
                    canvas.delete(aliens[i])
                    aliens[i] = None
                else:
                    i+=1
            else:
                i+=1

    return destruction

class missile:
    def __init__(self, coords):
        self.coords = coords

    def makeTir(self):
        global canvas
        #print self.coords
        self.xTir = self.coords[2] - ((self.coords[2] - self.coords[0]) / 2.0)
        self.yTir = self.coords[1]
        dimX = 10
        dimY = 20
        self.tir = canvas.create_rectangle(self.xTir - dimX / 2, self.yTir - dimY, self.xTir + dimX / 2, self.yTir, fill='YELLOW')


    def moveTir(self):
        global canvas, canvasWidth, canvasHeight, shoot
        pas = 30

        self.posTir = canvas.coords(self.tir)


        if self.posTir[3] <= 0 and self.tir:
            window.after_cancel(self.tir)
            canvas.delete(self.tir)
            killBoum()

        elif contact(self.posTir):
            window.after_cancel(self.tir)
            canvas.delete(self.tir)
            killBoum()

        else:
            canvas.coords(self.tir, self.posTir[0], self.posTir[1] - pas, self.posTir[2], self.posTir[3] - pas)
            window.after(20, self.moveTir)


def alien():
    global canvas, aliens, nbAliens, aLignes, aColonnes

    nbAliens = aLignes * aColonnes
    dimensionAlien = 50

    for i in range(1, aColonnes + 1):
        for j in range(1, aLignes + 1):
            aliens.append(canvas.create_rectangle(60 * i, 60 * j, 60 * i + dimensionAlien, 60 * j - dimensionAlien, fill='GREEN'))


def moveAlien():
    global canvas, aliens, canvasWidth, canvasHeight, nbAliens, direction
    pas = 5
    pasY = 20
    posY = 0
    alienGauche = canvas.coords(aliens[0])[0]
    alienDroite = canvas.coords(aliens[27])[2]
    if alienGauche - pas <= 0:
        direction = 1
        posY += pasY

    elif alienDroite + pas >= canvasWidth:
        direction = -1
        posY += pasY

    for i in range(nbAliens):
        if aliens[i] is not None:
            coordAlien = canvas.coords(aliens[i])
            canvas.coords(aliens[i], coordAlien[0] + direction * pas, coordAlien[1] + posY, coordAlien[2] + direction * pas, coordAlien[3] + posY)

    window.after(20, moveAlien)


def vaisseau():
    global canvas, canvasWidth, canvasHeight, vaisseau

    dimensionVaisseau = 45

    vaisseau = canvas.create_rectangle((canvasWidth / 2) - dimensionVaisseau / 2, canvasHeight - 10 - dimensionVaisseau, canvasWidth / 2 + dimensionVaisseau / 2, canvasHeight - 10, fill='BISQUE')


def restartGame():
    pass


createGUI()
alien()
vaisseau()
moveAlien()
window.mainloop()
